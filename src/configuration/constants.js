module.exports = {
  reasonCodes: {
    RECEIVED_INVALID_MESSAGE: 'ReceivedInvalidMessage',
    DATABASE_ERROR: 'DatabaseError',
    RABBIT_MQ_ERROR: 'RabbitMQError',
    SERVER_ERROR: 'ServerError',
    INCONSISTENT_STATE_ERROR: 'InconsistentStateError'
  },

  communicationTopology: {
    EXCHANGE_SAGA_TASK_ORDER: 'SagaTaskOrder',
    QUEUE_SAGA_TASK_ORDER: 'SagaTaskOrder_Queue',
    EXCHANGE_SAGA_EXECUTION: 'SagaExecution',
    QUEUE_SAGA_TASK: 'SagaExecution_Queue',
    EXCHANGE_SAGA_EXECUTION_ERROR: 'SagaExecution_Error',
    QUEUE_SAGA_TASK_ERROR: 'SagaExecution_Error_Queue',
    EXCHANGE_SAGA_INFORMATION: 'SagaInformation',
    QUEUE_SAGA_INFORMATION: 'SagaInformation_Queue',
    // TODO: Implement real sagas
    // EXCHANGE_SAGA_EXECUTION: 'SagaExecution',
    // QUEUE_SAGA_TASK: 'SagaExecution_Queue',
    // EXCHANGE_SAGA_TASK_ORDER: 'SagaTaskOrder',
    // QUEUE_SAGA_TASK_ORDER: 'SagaTaskOrder_Queue',
    // EXCHANGE_SAGA_INFORMATION: 'SagaInformation',
    // QUEUE_SAGA_INFORMATION: 'SagaInformation_Queue',
    // EXCHANGE_SAGA_INFORMATION_UPDATE: 'SagaInformationUpdate',
    // QUEUE_SAGA_INFORMATION_UPDATE: 'SagaInformationUpdate_Queue',
    // EXCHANGE_SAGA_RESUME: 'SagaResume',
    // QUEUE_SAGA_RESUME: 'SagaResume_Queue',
    ROUTING_KEY_UPDATE: 'update',
    ROUTING_KEY_NEW_TASK: 'newTask'
  },

  sagas: [
    {
      ex: 'Ticker_Saga',
      exStatus: 'Ticker_Saga_Status',
      queue: 'Ticker_Saga_Queue',
      consumer: 'sagaTickerService',
      handler: 'sagaTickerHandler'
    }
  ]
}
