module.exports = {
  kraken: {
    worker: {
      start: 'kraken.start',
      stop: 'kraken.stop'
    },
    aggregator: {
      start: 'aggregator.startkraken',
      stop: 'aggregator.stopkraken'
    }
  },
  binance: {
    worker: {
      start: 'binance.start',
      stop: 'binance.stop'
    },
    aggregator: {
      start: 'aggregator.startbinance',
      stop: 'aggregator.stopbinance'
    }
  },
  huobi: {
    worker: {
      start: 'huobi.start',
      stop: 'huobi.stop'
    },
    aggregator: {
      start: 'aggregator.starthuobi',
      stop: 'aggregator.stophuobi'
    }
  }
}
