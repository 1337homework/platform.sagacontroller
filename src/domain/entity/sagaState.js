const { constants } = require('../../configuration')

class SagaState {

  constructor(message) {
    this.message = message
    this.exchange = constants.sagas[0].ex
  }
}

module.exports = SagaState
