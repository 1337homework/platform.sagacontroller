const schema = (t, knex) => {
  t.increments('id').primary()
  t.string('sagaId')
  t.string('resourceId')
  t.string('serializedTask', 4096)
  t.datetime('createdAt', 6).defaultTo(knex.fn.now(6))
  t.datetime('updatedAt', 6).defaultTo(knex.fn.now(6))
}

exports.up = (knex, Promise) => {
  return Promise.all([
    knex.schema.createTable('sagaTaskState', (table) => schema(table, knex))
  ])
}

exports.down = (knex) => {
  return knex.schema.dropTableIfExists('sagaTaskState')
}