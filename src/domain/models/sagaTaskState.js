const { Model } = require('objection')

class SagaTaskState extends Model {
  static get tableName() {
    return 'sagaTaskState'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['sagaId', 'resourceId'],

      properties: {
        id: { type: 'integer' },
        sagaId: { type: 'string', minLength: 1, maxLength: 255 },
        resourceId: { type: 'string', minLength: 1, maxLength: 255 },
        serializedTask: { type: 'string', minLength: 1, maxLength: 4096 },
        createdAt: { type: 'string', minLength: 1, maxLength: 255 },
        updatedAt: { type: 'string', minLength: 1, maxLength: 255 }
      }
    }
  }

  static get relationMappings() {
    // implement if any
  }
}

module.exports = SagaTaskState
