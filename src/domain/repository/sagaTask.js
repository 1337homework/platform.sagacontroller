const moment = require('moment')
const SagaTaskState = require('../models/sagaTaskState')

const insertBasedOnEvent = async (eventMessage, resourceId) => {
  return await SagaTaskState
    .query()
    .insert({
      sagaId: eventMessage.sagaId,
      resourceId: resourceId,
      serializedTask: JSON.stringify(eventMessage),
      createdAt: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    })
}

const destroyTask = async (sagaId, resourceId) => {
  return await SagaTaskState
    .query()
    .delete()
    .where('sagaId', sagaId)
    .where('resourceId', resourceId)
}

module.exports = {
  insertBasedOnEvent,
  destroyTask
}
