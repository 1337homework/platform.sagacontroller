const { logger } = require('../../utils/logger')
const { communicationTopology } = require('../../configuration/constants')

// TODO: Implement
const sagaStatusUpdateHandler = (message) => {
  logger.log({ level: 'info', message: `New message received in ${communicationTopology.EXCHANGE_MARKET_ORDER}` })

  console.log(message)
}

module.exports = {
  sagaStatusUpdateHandler
}
