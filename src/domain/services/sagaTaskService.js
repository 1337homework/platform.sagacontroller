const uuid = require('uuid/v4')
const { logger } = require('../../utils/logger')
const routings = require('../../configuration/routings')
const { communicationTopology, reasonCodes } = require('../../configuration/constants')
const sagaTaskStateRepository = require('../repository/sagaTask')
const rabbit = require('../../interfaces/rabbit')
const errorHandler = require('../../utils/errorHandler')

const rollbackDb = async (originalMessage, message, resourceId, originalErrorEvent) => {
  logger.log({ level: 'info', message: 'Database rollback initiated.' })

  try {
    await sagaTaskStateRepository.destroyTask(message.sagaId, resourceId)
  } catch (err) {
    try {
      await errorHandler.resolveThroughNackAndRequeue(originalMessage)
    } catch (nerr) {
      errorHandler.notifyErrorUpstream(
        'Failed nack and requeue in rollback',
        reasonCodes.RABBIT_MQ_ERROR,
        originalMessage,
        originalErrorEvent,
        nerr
      )
    }
    errorHandler.notifyErrorUpstream(
      'Failed database rollback',
      reasonCodes.DATABASE_ERROR,
      originalMessage,
      originalErrorEvent,
      err
    )
  }

  logger.log({ level: 'info', message: 'Database rollback was completed.' })
}

const sagaTaskHandler = async (originalMessage) => {
  // workaround for this demo
  rabbit.publishToSagaStatusExchange({ status: 'OK' }, originalMessage.fields.routingKey)

  logger.log({ level: 'info', message: `New message received in ${communicationTopology.EXCHANGE_SAGA_EXECUTION}` })
  let message = null

  try {
    message = JSON.parse(originalMessage.content.toString('utf8'))
  } catch (err) {
    errorHandler.notifyErrorUpstream(
      'Error while trying to deserialize message',
      reasonCodes.RABBIT_MQ_ERROR,
      originalMessage,
      err
    )
    return
  }
  console.log(message)
  if (
    !message.exchangePlatform ||
    !message.taskType
  ) {
    errorHandler.notifyErrorUpstream(
      'Error while trying to deserialize message',
      reasonCodes.SERVER_ERROR,
      originalMessage,
      `Recieved message is, ${JSON.stringify(message)}`
    )
    return
  }

  //TODO: just for testing
  message.sagaId = uuid()

  if (!routings.hasOwnProperty(message.exchangePlatform)) {
    errorHandler.notifyErrorUpstream(
      'Error while trying to deserialize message',
      reasonCodes.SERVER_ERROR,
      originalMessage,
      `Cannot map exchange platform type, ${message.taskType}`
    )
    return
  }

  const resourceId = uuid()

  logger.log({
    level: 'info',
    message: `\
New message for saga ${message.sagaId} \
and task type ${message.taskType} \
has been successfully verified and assigned id ${resourceId}.`
  })

  logger.log({ level: 'info', message: 'Starting atomic Unit of work Database update and event publish.' })

  try {
    await sagaTaskStateRepository.insertBasedOnEvent(message, resourceId)
  } catch (err) {
    logger.log({ level: 'error', message: `Failed to update database. Trying to NACK and requeue. ${err}` })
    errorHandler.resolveThroughNackAndRequeue(originalMessage)
    return
  }

  logger.log({ level: 'info', message: 'Database was successfully updated. Publishing event related to state change.' })

  const messageToWorkers = Object.assign({}, message, { resourceId, originalMessageProps: originalMessage.properties })

  try {
    // this is now very wrong saga controller is fucked up, only for this demo...
    rabbit.publishToSagaOrderExchange(
      messageToWorkers,
      routings[message.exchangePlatform].aggregator[message.taskType]
    )

    rabbit.publishToSagaOrderExchange(
      messageToWorkers,
      routings[message.exchangePlatform].worker[message.taskType]
    )
  } catch (err) {
    logger.log({ level: 'error', message: `Failed to dispatch message to workers. Rollback neeeded. ${err}` })
    rollbackDb(originalMessage, message, resourceId, err)
    return
  }

  rabbit.ack(originalMessage)
}

module.exports = {
  sagaTaskHandler
}
