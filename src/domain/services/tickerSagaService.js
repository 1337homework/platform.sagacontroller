const rabbit = require('../../interfaces/rabbit')
const { reasonCodes } = require('../../configuration/constants')
const errorHandler = require('../../utils/errorHandler')
const handleSagaTasks = require('../../utils/sagaTasksHandler')
const { logger } = require('../../utils/logger')
const sagaStateRepository = require('../repository/sagaState')
const SagaState = require('../entity/sagaState')

const rollbackDb = () => {
  return true
}

// TODO: Fix & Implement
const tickerSagaHandler = async (originalMessage) => {
  let message

  try {
    message = JSON.parse(originalMessage.content.toString('utf8'))
  } catch (err) {
    errorHandler.notifyErrorUpstream('Malformed message', reasonCodes.SERVER_ERROR, originalMessage, err)
    return
  }

  let sagaState

  try {
    sagaState = new SagaState(message)
  } catch (err) {
    errorHandler.notifyErrorUpstream('Saga creation failed', reasonCodes.SERVER_ERROR, originalMessage, err)
    return
  }

  logger.log({ level: 'info', message: 'New saga created. Saving saga in Database.' })

  try {
    await sagaStateRepository.insertNewSaga(sagaState)
  } catch (err) {
    rollbackDb(err)
    return
  }

  logger.log({ level: 'info', message: 'New saga saved. Firing events.' })

  try {
    await handleSagaTasks(sagaState.tasks)
  } catch (err) {
    rollbackDb(err)
    return
  }

  logger.log({ level: 'info', message: 'Saga is started successfully.' })

  // TODO: implement
  rabbit.publishToSagaStateStatusExchange(
    sagaState.exchange,
    { sagaId: sagaState.sagaId },
    originalMessage.fields.routingKey
  )
}

module.exports = {
  tickerSagaHandler
}
