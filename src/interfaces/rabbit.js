const amqp = require('amqplib')
const {
  rabbit,
  constants
} = require('../configuration')
const { logger } = require('../utils/logger')

let rabbitChannel = null

const publishToExchange = (exchange, msg, routingKey) => {
  return rabbitChannel.publish(
    exchange,
    routingKey,
    Buffer.from(JSON.stringify(msg)),
    {
      persistent: true,
      contentType: 'application/json'
    }
  )
}

const publishToSagaExecutionExchange = (msg) => {
  return publishToExchange(
    constants.communicationTopology.EXCHANGE_SAGA_EXECUTION,
    msg,
    constants.communicationTopology.ROUTING_KEY_UPDATE
  )
}

const publishToSagaExecutionErrorExchange = (msg) => {
  return publishToExchange(
    constants.communicationTopology.EXCHANGE_SAGA_EXECUTION_ERROR,
    msg
  )
}

const publishToSagaOrderExchange = (msg, routingKey) => {
  return publishToExchange(constants.communicationTopology.EXCHANGE_SAGA_TASK_ORDER, msg, routingKey)
}

const publishToSagaStatusExchange = (msg, routingKey) => {
  return publishToExchange(constants.communicationTopology.EXCHANGE_SAGA_INFORMATION, msg, routingKey)
}

const registerConsumer = (queue, consumer) => {
  return rabbitChannel.consume(queue, consumer)
}

const registerSagaConsumers = () => {
  return new Promise((resolve) => {
    constants.communicationTopology.sagas.forAch((saga) => {
      const consumer = require(`../domain/services/${saga.consumer}`)
      resolve(registerConsumer(saga.queue, consumer[saga.handler]))
    })
  })
}

const registerSagaTaskConsumer = (consumer) => {
  return registerConsumer(constants.communicationTopology.QUEUE_SAGA_TASK, consumer)
}

const registerSagaTaskOrderConsumer = (consumer) => {
  return registerConsumer(constants.communicationTopology.QUEUE_SAGA_TASK_ORDER, consumer)
}

const registerSagaInformationConsumer = (consumer) => {
  return registerConsumer(constants.communicationTopology.QUEUE_SAGA_INFORMATION, consumer)
}

const registerSagaResumeConsumer = (consumer) => {
  return registerConsumer(constants.communicationTopology.QUEUE_SAGA_RESUME, consumer)
}

const assertSagaTaskChannelState = () => {
  return rabbitChannel.assertExchange(
    constants.communicationTopology.EXCHANGE_SAGA_EXECUTION,
    'topic',
    { durable: true }
  )
    .then(() => {
      return rabbitChannel.assertQueue(constants.communicationTopology.QUEUE_SAGA_TASK, { durable: true })
    })
    .then((result) => {
      return rabbitChannel.bindQueue(
        result.queue,
        constants.communicationTopology.EXCHANGE_SAGA_EXECUTION,
        '#'
      )
    })
}

const assertTaskStartChannelState = () => {
  return rabbitChannel.assertExchange(
    constants.communicationTopology.EXCHANGE_SAGA_TASK_ORDER,
    'topic',
    { durable: true }
  )
    .then(() => {
      return rabbitChannel.assertQueue(constants.communicationTopology.QUEUE_SAGA_TASK_ORDER, { durable: true })
    })
    .then((result) => {
      return rabbitChannel.bindQueue(
        result.queue,
        constants.communicationTopology.EXCHANGE_SAGA_TASK_ORDER,
        '#'
      )
    })
}

const assertSagaExecutionErrorChannelState = () => {
  return rabbitChannel.assertExchange(
    constants.communicationTopology.EXCHANGE_SAGA_EXECUTION_ERROR,
    'topic',
    { durable: false }
  )
    .then(() => {
      return rabbitChannel.assertQueue(constants.communicationTopology.QUEUE_SAGA_TASK_ERROR, { durable: true })
    })
}

const assertSagaResumeChannelState = () => {
  return rabbitChannel.assertExchange(
    constants.communicationTopology.EXCHANGE_SAGA_TASK_ORDER,
    'topic',
    { durable: false }
  )
    .then(() => {
      return rabbitChannel.assertExchange(
        constants.communicationTopology.EXCHANGE_SAGA_RESUME,
        'topic', { durable: false }
      )
    })
}

const assertSagaInformationChannelState = () => {
  return rabbitChannel.assertExchange(
    constants.communicationTopology.EXCHANGE_SAGA_INFORMATION,
    'topic',
    { durable: false }
  )
}

const assertSagasChannelState = () => {
  return constants.sagas.forEach((saga) => {
    return rabbitChannel.assertExchange(saga.ex, 'topic', { durable: false })
      .then(() => {
        return rabbitChannel.assertExchange(saga.exStatus, 'topic', { durable: false })
      })
      .then(() => {
        return rabbitChannel.assertQueue(saga.queue, { durable: false })
      })
  })
}

const openConnection = () => {
  return amqp.connect(rabbit.connStr)
    .then((conn) => {
      return conn.createChannel()
    })
    .then((channel) => {
      rabbitChannel = channel
      return Promise.all([
        assertSagaTaskChannelState(),
        assertTaskStartChannelState(),
        assertSagaExecutionErrorChannelState(),
        assertSagaInformationChannelState()
        //assertSagaResumeChannelState(),
        //assertSagaInformationChannelState(),
        //assertSagasChannelState(),
        //registerSagaConsumers()
      ])
    })
    .catch((err) => {
      logger.log({ level: 'error', message: `Failed connect to RabbitMQ, reconnecting... ${JSON.stringify(err)}` })
      return new Promise((resolve) => {
        setTimeout(() => resolve(openConnection()), rabbit.retryInterval)
      })
    })
}

const ack = (message, allUpTo = false) => {
  rabbitChannel.ack(message, allUpTo)
}

const nack = (message, multiple = false, requeue = false) => {
  rabbitChannel.nack(message, multiple, requeue)
}

module.exports = {
  openConnection,

  registerSagaTaskConsumer,
  // registerSagaInformationConsumer,
  // registerSagaResumeConsumer,
  // registerSagaTaskOrderConsumer,

  publishToSagaExecutionExchange,
  publishToSagaExecutionErrorExchange,
  publishToSagaOrderExchange,
  publishToSagaStatusExchange,

  ack,
  nack
}
