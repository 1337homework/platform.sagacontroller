const Knex = require('knex')
const { Model } = require('objection')
const { database } = require('../configuration')
const { logger } = require('../utils/logger')

/**
 * Connection to database
 * @returns {Promise} Connection promise
 */
async function connectToDb() {
  const knex = Knex(database)

  try {
    await knex.select(knex.raw('1'))
    return Promise.resolve(knex)
  } catch (err) {
    return Promise.reject(err)
  }
}

const openConnection = () => {
  return new Promise((resolve) => {
    return connectToDb()
      .then((knex) => {
        Model.knex(knex)
        resolve()
      })
      .catch((err) => {
        logger.log({ level: 'error', message: `Failed connect to database, reconnecting... ${JSON.stringify(err)}` })
        setTimeout(() => resolve(openConnection()), database.retryInterval)
      })
  })
}

module.exports = {
  openConnection
}
