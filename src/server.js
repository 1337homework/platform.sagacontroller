require('./configuration/database')

const rabbitmq = require('./interfaces/rabbit')
const db = require('./interfaces/sql')

const { logger } = require('./utils/logger')
const { sagaTaskHandler } = require('./domain/services/sagaTaskService')
// const { sagaResumeHandler } = require('./domain/services/sagaResumeService')
// const { sagaOrderHandler } = require('./domain/services/sagaOrderService')
// const { sagaStatusUpdateHandler } = require('./domain/services/sagaStatusUpdateService')

Promise
  .all([rabbitmq.openConnection(), db.openConnection()])
  .then(() => {
    rabbitmq.registerSagaTaskConsumer(sagaTaskHandler)
    // rabbitmq.registerSagaTaskOrderConsumer(sagaOrderHandler)
    // rabbitmq.registerSagaInformationConsumer(sagaStatusUpdateHandler)
    // rabbitmq.registerSagaResumeConsumer(sagaResumeHandler)
    logger.log({ level: 'info', message: 'Saga Controller Connected to MySQL and RabbitMQ. Serving...' })
  })
  .catch((err) => {
    logger.log({ level: 'error', message: `${err.message} - ${err.stack}` })
    process.exit(1)
  })
