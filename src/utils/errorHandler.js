const rabbitmq = require('../interfaces/rabbit')
const { reasonCodes } = require('../configuration/constants')
const { logger } = require('./logger')

const notifyUpstream = (error) => {
  return new Promise((resolve, reject) => {
    try {
      rabbitmq.publishToSagaExecutionErrorExchange(error)
      if (error.details) {
        rabbitmq.ack(error.details.originalMessage, false)
      }
      resolve()
    } catch (err) {
      logger.log({ level: 'error', message: `Notify upstream failed. ${err}` })
      reject()
    }
  })
}

const notifyErrorUpstream = async (
  reason,
  reasonCode = reasonCodes.SERVER_ERRORl,
  originalMessage = {},
  originalErrorEvent = {},
  errorEvent = null
) => {
  logger.log({ level: 'error', message: `${reasonCode} - ${reason}. ${originalErrorEvent} ${errorEvent}` })

  let error = {}
  let message = {}

  if (originalErrorEvent.message && originalErrorEvent.stack) {
    error = {
      errorMessage: originalErrorEvent.message || errorEvent.message,
      errorStack: originalErrorEvent.stack || errorEvent.stack
    }
  } else if (errorEvent && errorEvent.message && errorEvent.stack) {
    error = {
      errorMessage: errorEvent.message,
      errorStack: errorEvent.stack
    }
  }

  try {
    message = JSON.parse(originalMessage.content.toString('utf8'))
  } catch (e) {
    logger.log({ level: 'error', message: `Error in error handler. Malformed original message.` })
  }

  error = {
    reasonCode,
    reason,
    sagaId: message.sagaId,
    taskName: message.taskName,
    taskType: message.taskType,
    resourceId: message.resourceId,
    details: {
      originalMessage,
      ...error
    }
  }

  return await notifyUpstream(error)
}

const resolveThroughNackAndRequeue = async (originalMessage) => {
  try {
    if (originalMessage.fields.redelivered) {
      rabbitmq.nack(originalMessage.details.originalMessage, false, true)
    } else {
      await notifyUpstream('This error already redelivered', reasonCodes.RECEIVED_INVALID_MESSAGE, originalMessage)
    }
  } catch (e) {
    logger.log({ level: 'error', message: 'Resolve through NACK and Requeue failed.' })
    return
  }
}

module.exports = {
  notifyErrorUpstream,
  resolveThroughNackAndRequeue
}
